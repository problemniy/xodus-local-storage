package com.web.localstorage.app.database.config;

import jetbrains.exodus.entitystore.PersistentEntityStore;
import jetbrains.exodus.entitystore.PersistentEntityStores;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class XodusDbConfiguration {

    private static final String TEST_PROFILE_TAG = "test";
    private static final String XODUS_DB_PATH = "/xodus";

    @Profile("!" + TEST_PROFILE_TAG)
    @Bean(value = "persistentEntityStore")
    public PersistentEntityStore getPersistentEntityStore(@Value("${xodus.db.path:" + XODUS_DB_PATH + "}") String xodusDbPath) {
        return PersistentEntityStores.newInstance(xodusDbPath);
    }

    @Profile(TEST_PROFILE_TAG)
    @Bean(value = "persistentEntityStore")
    public PersistentEntityStore getTestPersistentEntityStore(@Value("${xodus.db.path:" + XODUS_DB_PATH + "}") String xodusDbPath) {
        PersistentEntityStore persistentEntityStore = PersistentEntityStores.newInstance(xodusDbPath);
        persistentEntityStore.clear();
        return persistentEntityStore;
    }
}
