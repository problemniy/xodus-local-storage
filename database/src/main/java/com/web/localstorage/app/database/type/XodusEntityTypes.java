package com.web.localstorage.app.database.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum XodusEntityTypes {
    NUMBER("number"), STRING("string"), FLAG("flag");

    private final String type;
}
