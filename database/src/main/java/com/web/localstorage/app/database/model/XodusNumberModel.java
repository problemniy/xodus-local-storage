package com.web.localstorage.app.database.model;

import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@SuperBuilder
public class XodusNumberModel extends AbstractXodusModel<Long> {

}
