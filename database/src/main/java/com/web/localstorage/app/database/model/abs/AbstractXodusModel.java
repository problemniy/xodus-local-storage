package com.web.localstorage.app.database.model.abs;

import com.web.localstorage.app.database.validation.XodusModelValidation;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractXodusModel<T extends Comparable<?>> {

    public static final String DATA_KEY = "data";

    @NotBlank(groups = {XodusModelValidation.Id.class})
    protected String id;

    @NotNull(groups = {XodusModelValidation.Data.class})
    protected T data;
}
