package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusNumberModel;
import com.web.localstorage.app.manage.repository.XodusNumberEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusEntitiesRestControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(XodusRestNumberEntitiesController.class)
class XodusRestNumberEntitiesControllerTest extends AbstractXodusEntitiesRestControllerTest<XodusNumberModel, XodusNumberEntitiesRepository, Long> {

    private static final String CONTROLLER_URI = "/entities/numbers";

    @Autowired
    private XodusNumberEntitiesRepository numberEntitiesRepository;

    @Override
    protected String getControllerUri() {
        return CONTROLLER_URI;
    }

    @Override
    protected XodusNumberEntitiesRepository getEntityRepository() {
        return numberEntitiesRepository;
    }

    @Test
    void getAllEntityTest() throws Exception {
        final long INSERT_DATA_1 = 100500L;
        final long INSERT_DATA_2 = 110300L;

        getAllEntityTest(INSERT_DATA_1, INSERT_DATA_2);
    }

    @Test
    void createEntityTest() throws Exception {
        final long INSERT_DATA = 100500L;

        createEntityTest(INSERT_DATA, XodusNumberModel.class);
    }

    @Test
    void getEntityTest() throws Exception {
        final long INSERT_DATA = 100700L;

        getEntityTest(INSERT_DATA);
    }

    @Test
    void updateEntityTest() throws Exception {
        final long INSERT_DATA = 100900L;
        final long UPDATE_DATA = 4200L;

        updateEntityTest(INSERT_DATA, UPDATE_DATA, XodusNumberModel.class);
    }

    @Test
    void deleteEntityTest() throws Exception {
        final long INSERT_DATA = 101100L;

        deleteEntityTest(INSERT_DATA);
    }
}