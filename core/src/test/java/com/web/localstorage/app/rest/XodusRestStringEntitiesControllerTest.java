package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusStringModel;
import com.web.localstorage.app.manage.repository.XodusStringEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusEntitiesRestControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(XodusRestStringEntitiesController.class)
class XodusRestStringEntitiesControllerTest extends AbstractXodusEntitiesRestControllerTest<XodusStringModel, XodusStringEntitiesRepository, String> {

    private static final String CONTROLLER_URI = "/entities/strings";

    @Autowired
    private XodusStringEntitiesRepository stringEntitiesRepository;

    @Override
    protected String getControllerUri() {
        return CONTROLLER_URI;
    }

    @Override
    protected XodusStringEntitiesRepository getEntityRepository() {
        return stringEntitiesRepository;
    }

    @Test
    void getAllEntityTest() throws Exception {
        final String INSERT_DATA_1 = "Hello";
        final String INSERT_DATA_2 = "Bye";

        getAllEntityTest(INSERT_DATA_1, INSERT_DATA_2);
    }

    @Test
    void createEntityTest() throws Exception {
        final String INSERT_DATA = "Hello";

        createEntityTest(INSERT_DATA, XodusStringModel.class);
    }

    @Test
    void getEntityTest() throws Exception {
        final String INSERT_DATA = "Hello";

        getEntityTest(INSERT_DATA);
    }

    @Test
    void updateEntityTest() throws Exception {
        final String INSERT_DATA = "Hello";
        final String UPDATE_DATA = "Bye";

        updateEntityTest(INSERT_DATA, UPDATE_DATA, XodusStringModel.class);
    }

    @Test
    void deleteEntityTest() throws Exception {
        final String INSERT_DATA = "Hello";

        deleteEntityTest(INSERT_DATA);
    }
}