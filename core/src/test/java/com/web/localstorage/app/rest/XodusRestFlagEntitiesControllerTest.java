package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusFlagModel;
import com.web.localstorage.app.manage.repository.XodusFlagEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusEntitiesRestControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(XodusRestFlagEntitiesController.class)
class XodusRestFlagEntitiesControllerTest extends AbstractXodusEntitiesRestControllerTest<XodusFlagModel, XodusFlagEntitiesRepository, Boolean> {

    private static final String CONTROLLER_URI = "/entities/flags";

    @Autowired
    private XodusFlagEntitiesRepository flagEntitiesRepository;

    @Override
    protected String getControllerUri() {
        return CONTROLLER_URI;
    }

    @Override
    protected XodusFlagEntitiesRepository getEntityRepository() {
        return flagEntitiesRepository;
    }

    @Test
    void getAllEntityTest() throws Exception {
        final boolean INSERT_DATA_1 = true;
        final boolean INSERT_DATA_2 = false;

        getAllEntityTest(INSERT_DATA_1, INSERT_DATA_2);
    }

    @Test
    void createEntityTest() throws Exception {
        final boolean INSERT_DATA = true;

        createEntityTest(INSERT_DATA, XodusFlagModel.class);
    }

    @Test
    void getEntityTest() throws Exception {
        final boolean INSERT_DATA = false;

        getEntityTest(INSERT_DATA);
    }

    @Test
    void updateEntityTest() throws Exception {
        final boolean INSERT_DATA = false;
        final boolean UPDATE_DATA = true;

        updateEntityTest(INSERT_DATA, UPDATE_DATA, XodusFlagModel.class);
    }

    @Test
    void deleteEntityTest() throws Exception {
        final boolean INSERT_DATA = true;

        deleteEntityTest(INSERT_DATA);
    }
}