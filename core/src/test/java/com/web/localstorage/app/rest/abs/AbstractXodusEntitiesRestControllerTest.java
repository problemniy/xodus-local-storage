package com.web.localstorage.app.rest.abs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import com.web.localstorage.app.loader.YamlPropertiesLoaderFactory;
import com.web.localstorage.app.manage.aspect.XodusTransactionManagerAspect;
import com.web.localstorage.app.manage.repository.XodusEntitiesRepository;
import jakarta.annotation.Nullable;
import jetbrains.exodus.entitystore.PersistentEntityStore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ComponentScan({
        "com.web.localstorage.app.manage",
        "com.web.localstorage.app.database"
})
@Import({AopAutoConfiguration.class, XodusTransactionManagerAspect.class})
@TestPropertySource(locations = "/xodus.test.config.yml", factory = YamlPropertiesLoaderFactory.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractXodusEntitiesRestControllerTest<T extends AbstractXodusModel<A>, R extends XodusEntitiesRepository<T, A>, A extends Comparable<?>> {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    private PersistentEntityStore persistentEntityStore;

    protected final ObjectMapper jsonMapper = new ObjectMapper();

    @AfterAll
    protected void teardown() {
        persistentEntityStore.close();
    }

    protected abstract String getControllerUri();

    protected abstract R getEntityRepository();

    protected void getAllEntityTest(A expectData1, A expectData2) throws Exception {
        T model1 = createEntity(expectData1);
        T model2 = createEntity(expectData2);

        try {
            mockMvc.perform(MockMvcRequestBuilders
                            .get(getControllerUri())
                            .session(new MockHttpSession())
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$.*.id", everyItem(notNullValue())))
                    .andExpect(jsonPath("$.*.data", everyItem(notNullValue())));
        } finally {
            removeEntity(model1 != null ? model1.getId() : null);
            removeEntity(model2 != null ? model2.getId() : null);
        }
    }

    protected void createEntityTest(A expectData, Class<T> modelClass) throws Exception {
        T model = modelClass.getDeclaredConstructor().newInstance();
        model.setData(expectData);
        T createdModel = null;
        try {
            String jsonBody = jsonMapper.writeValueAsString(model);
            ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                    .post(getControllerUri())
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(jsonBody));
            String resultStr = resultActions.andReturn().getResponse().getContentAsString();

            resultActions
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("$.id").isNotEmpty())
                    .andExpect(jsonPath("$.data").value(expectData));

            createdModel = jsonMapper.readValue(resultStr, modelClass);

            model.setData(null);
            jsonBody = jsonMapper.writeValueAsString(model);
            mockMvc.perform(MockMvcRequestBuilders
                            .post(getControllerUri())
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(jsonBody))
                    .andExpect(status().isBadRequest());
        } finally {
            removeEntity(createdModel != null ? createdModel.getId() : null);
        }
    }

    protected void getEntityTest(A expectData) throws Exception {
        T model = null;
        try {
            mockMvc.perform(MockMvcRequestBuilders
                            .get(getControllerUri() + "/0-0")
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound());

            model = createEntity(expectData);

            assertNotNull(model.getId());
            assertEquals(model.getData(), expectData);

            mockMvc.perform(MockMvcRequestBuilders
                            .get(getControllerUri() + "/" + model.getId())
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(model.getId()))
                    .andExpect(jsonPath("$.data").value(model.getData()));

            mockMvc.perform(MockMvcRequestBuilders
                            .get(getControllerUri() + "/")
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound());
        } finally {
            removeEntity(model != null ? model.getId() : null);
        }
    }

    protected void updateEntityTest(A insertData, A expectData, Class<T> modelClass) throws Exception {
        T model = null;
        try {
            T tempModel = modelClass.getDeclaredConstructor().newInstance();
            tempModel.setData(insertData);

            String jsonBody = jsonMapper.writeValueAsString(tempModel);
            mockMvc.perform(MockMvcRequestBuilders
                            .put(getControllerUri())
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(jsonBody))
                    .andExpect(status().isBadRequest());

            tempModel.setId("0-0");
            jsonBody = jsonMapper.writeValueAsString(tempModel);
            mockMvc.perform(MockMvcRequestBuilders
                            .put(getControllerUri())
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(jsonBody))
                    .andExpect(status().isNotFound());

            model = createEntity(insertData);

            assertNotNull(model.getId());
            assertEquals(model.getData(), insertData);

            model.setData(expectData);
            jsonBody = jsonMapper.writeValueAsString(model);
            mockMvc.perform(MockMvcRequestBuilders
                            .put(getControllerUri())
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(jsonBody))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(model.getId()))
                    .andExpect(jsonPath("$.data").value(model.getData()));
        } finally {
            removeEntity(model != null ? model.getId() : null);
        }
    }

    protected void deleteEntityTest(A expectData) throws Exception {
        T model = null;
        boolean result = false;
        try {
            mockMvc.perform(MockMvcRequestBuilders
                            .delete(getControllerUri() + "/"))
                    .andExpect(status().isNotFound());

            mockMvc.perform(MockMvcRequestBuilders
                            .delete(getControllerUri() + "/0-0"))
                    .andExpect(status().isNotFound());

            model = createEntity(expectData);

            assertNotNull(model.getId());
            assertEquals(model.getData(), expectData);

            mockMvc.perform(MockMvcRequestBuilders
                            .delete(getControllerUri() + "/" + model.getId()))
                    .andExpect(status().isNoContent())
                    .andReturn().getResponse().getContentAsString();
            result = true;
        } finally {
            if (!result) {
                removeEntity(model != null ? model.getId() : null);
            }
        }
    }

    protected T createEntity(A dataValue) {
        return getEntityRepository().createEntityAndFlush(dataValue).orElseThrow();
    }

    protected void removeEntity(@Nullable String key) {
        if (key != null) {
            getEntityRepository().removeEntityAndFlush(key);
        }
    }
}
