package com.web.localstorage.app;

import com.web.localstorage.app.loader.YamlPropertiesLoaderFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@PropertySources({
        @PropertySource(value = "classpath:xodus.config.yml", factory = YamlPropertiesLoaderFactory.class)
})
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "yaml")
@SpringBootApplication
public class XodusLocalStorageApp {

    public static void main(String[] args) {
        SpringApplication.run(XodusLocalStorageApp.class, args);
    }
}