package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusNumberModel;
import com.web.localstorage.app.database.validation.XodusModelValidation;
import com.web.localstorage.app.manage.repository.XodusNumberEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusRestEntitiesController;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("entities/numbers")
public class XodusRestNumberEntitiesController extends AbstractXodusRestEntitiesController<XodusNumberModel, Long> {

    private final XodusNumberEntitiesRepository numberEntitiesRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<Collection<XodusNumberModel>> getAllEntity() {
        return new ResponseEntity<>(numberEntitiesRepository.getAllEntities(), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusNumberModel> createEntity(@RequestBody @Validated(XodusModelValidation.Data.class) XodusNumberModel transientModel) {
        return new ResponseEntity<>(numberEntitiesRepository.createEntity(transientModel.getData())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST)),
                HttpStatus.CREATED);
    }

    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusNumberModel> getEntity(@NotBlank @PathVariable("key") String key) {
        return new ResponseEntity<>(numberEntitiesRepository.getEntity(key)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusNumberModel> updateEntity(@RequestBody @Validated({
            XodusModelValidation.Id.class, XodusModelValidation.Data.class
    }) XodusNumberModel transientModel) {
        return new ResponseEntity<>(numberEntitiesRepository.updateEntity(transientModel)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @DeleteMapping("/{key}")
    @Override
    public ResponseEntity<Boolean> deleteEntity(@NotBlank @PathVariable("key") String key) {
        if (numberEntitiesRepository.removeEntity(key)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
