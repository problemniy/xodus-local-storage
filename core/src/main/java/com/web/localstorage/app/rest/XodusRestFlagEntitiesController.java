package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusFlagModel;
import com.web.localstorage.app.database.validation.XodusModelValidation;
import com.web.localstorage.app.manage.repository.XodusFlagEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusRestEntitiesController;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("entities/flags")
public class XodusRestFlagEntitiesController extends AbstractXodusRestEntitiesController<XodusFlagModel, Boolean> {

    private final XodusFlagEntitiesRepository flagEntitiesRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<Collection<XodusFlagModel>> getAllEntity() {
        return new ResponseEntity<>(flagEntitiesRepository.getAllEntities(), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusFlagModel> createEntity(@RequestBody @Validated(XodusModelValidation.Data.class) XodusFlagModel transientModel) {
        return new ResponseEntity<>(flagEntitiesRepository.createEntity(transientModel.getData())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST)),
                HttpStatus.CREATED);
    }

    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusFlagModel> getEntity(@NotBlank @PathVariable("key") String key) {
        return new ResponseEntity<>(flagEntitiesRepository.getEntity(key)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusFlagModel> updateEntity(@RequestBody @Validated({
            XodusModelValidation.Id.class, XodusModelValidation.Data.class
    }) XodusFlagModel transientModel) {
        return new ResponseEntity<>(flagEntitiesRepository.updateEntity(transientModel)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @DeleteMapping("/{key}")
    @Override
    public ResponseEntity<Boolean> deleteEntity(@NotBlank @PathVariable("key") String key) {
        if (flagEntitiesRepository.removeEntity(key)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
