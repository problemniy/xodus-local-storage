package com.web.localstorage.app.rest.abs;

import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import jakarta.validation.constraints.NotBlank;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/entities")
public abstract class AbstractXodusRestEntitiesController<T extends AbstractXodusModel<A>, A extends Comparable<?>> {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public abstract ResponseEntity<Collection<T>> getAllEntity();

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public abstract ResponseEntity<T> createEntity(@RequestBody T transientModel);

    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public abstract ResponseEntity<T> getEntity(@NotBlank @PathVariable("key") String key);

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public abstract ResponseEntity<T> updateEntity(@RequestBody T transientModel);

    @DeleteMapping("/{key}")
    public abstract ResponseEntity<Boolean> deleteEntity(@NotBlank @PathVariable("key") String key);
}
