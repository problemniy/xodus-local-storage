package com.web.localstorage.app.rest;

import com.web.localstorage.app.database.model.XodusStringModel;
import com.web.localstorage.app.database.validation.XodusModelValidation;
import com.web.localstorage.app.manage.repository.XodusStringEntitiesRepository;
import com.web.localstorage.app.rest.abs.AbstractXodusRestEntitiesController;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("entities/strings")
public class XodusRestStringEntitiesController extends AbstractXodusRestEntitiesController<XodusStringModel, String> {

    private final XodusStringEntitiesRepository stringEntitiesRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<Collection<XodusStringModel>> getAllEntity() {
        return new ResponseEntity<>(stringEntitiesRepository.getAllEntities(), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusStringModel> createEntity(@RequestBody @Validated(XodusModelValidation.Data.class) XodusStringModel transientModel) {
        return new ResponseEntity<>(stringEntitiesRepository.createEntity(transientModel.getData())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST)),
                HttpStatus.CREATED);
    }

    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusStringModel> getEntity(@NotBlank @PathVariable("key") String key) {
        return new ResponseEntity<>(stringEntitiesRepository.getEntity(key)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<XodusStringModel> updateEntity(@RequestBody @Validated({
            XodusModelValidation.Id.class, XodusModelValidation.Data.class
    }) XodusStringModel transientModel) {
        return new ResponseEntity<>(stringEntitiesRepository.updateEntity(transientModel)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)),
                HttpStatus.OK);
    }

    @DeleteMapping("/{key}")
    @Override
    public ResponseEntity<Boolean> deleteEntity(@NotBlank @PathVariable("key") String key) {
        if (stringEntitiesRepository.removeEntity(key)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
