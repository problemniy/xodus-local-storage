## Xodus Local Storage

![Project Status: Active](https://img.shields.io/badge/Project_Status-Active-darkgreen.svg)
![Spring Boot](https://img.shields.io/badge/-Spring%20Boot-6DB33F?logo=spring-boot&logoColor=white)
![Java](https://img.shields.io/badge/Java-%23ED8B00.svg?logo=openjdk&logoColor=white)
![Docker](https://img.shields.io/badge/-Docker-2496ED?logo=docker&logoColor=white)
![Gradle](https://img.shields.io/badge/-Gradle-02303A?logo=gradle&logoColor=white)

A testing server application providing local storage using the local NoSQL [Xodus](https://github.com/JetBrains/xodus)
database and implementing a custom transaction manager

- Implements a custom transaction manager based on the AspectJ library in Spring
- Works with the JetBrains Xodus database
- Provides CRUD operations for three types of objects: *Numbers, Flags, Strings*

## Hello Worlds!

To deploy the application, you need to:

- Use Java 21 version
- Use Docker
- Use Postman

Features:

- Provides an [@XodusTransaction](manage/src/main/java/com/web/localstorage/app/manage/annotation/XodusTransaction.java)
  annotation indicating the necessity to open a transaction before calling the method

Examples:

```java

@XodusTransaction
public boolean removeEntity(String id) {
    // some logic
}

@XodusTransaction(XodusTransactionType.READ)
public Optional<XodusNumberModel> getEntity(String id) {
    // some logic
}
```

The type of transaction is specified as an argument
via [XodusTransactionType](manage/src/main/java/com/web/localstorage/app/manage/annotation/XodusTransactionType.java)

## Building from Source

[Gradle](https://www.gradle.org) is used to build, test, and publish. JDK 1.21 or higher is required.
<br>To build the project, run:

    ./gradlew clean build

- Upload the [postman-collection](core/src/main/resources/json/postman/xodus_local_storage.postman_collection.json)
  script to the Postman
  _<br>(you can simply use drag & drop)_


- Build the Docker image and run the container, use:

```cli
  docker build . -t xodus-local-storage
  docker run -p 8080:8080 -v ${PWD}/xodus:/app/xodus xodus-local-storage
```

## Notes

The Xodus data will be located in the project root

```
  ./xodus/db         // main data
  ./xodus/test/db    // test data used for integration tests
```

To test basic CRUD operations with various entity types, you can use [Postman](https://www.postman.com) by uploading a
prepared collection from the [core](core/src/main/resources/json/postman) resource package beforehand
<br><br>This will generate a set of REST requests for three supported types

```
Xodus Local Store
  - Numbers
      ... (CRUD requests)
  - Flags
      ... (CRUD requests)
  - Strings
      ... (CRUD requests)
```

, after which you will be able to interact with them and verify the database operations

## Author

@problemniy
