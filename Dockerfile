# Run 'gradle buildJars' before creating a Docker container
# Create a Docker image: ' docker build . -t xodus-local-storage '
# Run a Docker container: ' docker run -p 8080:8080 -v ${PWD}/xodus:/app/xodus xodus-local-storage '

FROM gradle:jdk21-alpine

WORKDIR /app

VOLUME /app/xodus

EXPOSE 8080

ENV MAIN_CLASS_NAME=com.web.localstorage.app.XodusLocalStorageApp

COPY ./build/app .

ENTRYPOINT exec java -cp "./*:lib/*" $MAIN_CLASS_NAME