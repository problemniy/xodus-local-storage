package com.web.localstorage.app.manage.repository;

import com.web.localstorage.app.database.model.XodusFlagModel;
import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import com.web.localstorage.app.database.type.XodusEntityTypes;
import com.web.localstorage.app.manage.annotation.XodusTransaction;
import com.web.localstorage.app.manage.service.XodusStoreTransactionService;
import jakarta.annotation.Nullable;
import jetbrains.exodus.entitystore.Entity;
import jetbrains.exodus.entitystore.EntityRemovedInDatabaseException;
import jetbrains.exodus.entitystore.StoreTransaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Repository
@Slf4j
@RequiredArgsConstructor
public class XodusFlagEntitiesRepository implements XodusEntitiesRepository<XodusFlagModel, Boolean> {

    private final XodusStoreTransactionService transactionService;

    @Override
    public Collection<XodusFlagModel> getAllEntities() {
        return getAllEntities(false);
    }

    @Override
    public Collection<XodusFlagModel> getAllEntitiesAndFlush() {
        return getAllEntities(true);
    }

    private Collection<XodusFlagModel> getAllEntities(boolean individualTransaction) {
        Collection<XodusFlagModel> models = new ArrayList<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> storeTransaction.getAll(XodusEntityTypes.FLAG.getType())
                .forEach(entity -> models.add(convertToFlagModel(entity)));

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return models;
    }

    @XodusTransaction
    @Override
    public Optional<XodusFlagModel> createEntity(Boolean value) {
        return createEntity(value, false);
    }

    @Override
    public Optional<XodusFlagModel> createEntityAndFlush(Boolean value) {
        return createEntity(value, true);
    }

    private Optional<XodusFlagModel> createEntity(Boolean value, boolean individualTransaction) {
        AtomicReference<Optional<XodusFlagModel>> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = storeTransaction.newEntity(XodusEntityTypes.FLAG.getType());
            entity.setProperty(AbstractXodusModel.DATA_KEY, value);
            model.set(Optional.ofNullable(convertToFlagModel(entity)));
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return model.get();
    }

    @XodusTransaction
    @Override
    public Optional<XodusFlagModel> getEntity(String id) {
        return getEntity(id, false);
    }

    @Override
    public Optional<XodusFlagModel> getEntityAndFlush(String id) {
        return getEntity(id, true);
    }

    private Optional<XodusFlagModel> getEntity(String id, boolean individualTransaction) {
        AtomicReference<XodusFlagModel> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = getXodusEntity(id, storeTransaction);

            if (entity != null && XodusEntityTypes.FLAG.getType().equals(entity.getType())) {
                model.set(convertToFlagModel(entity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(model.get());
    }

    @XodusTransaction
    @Override
    public Optional<XodusFlagModel> updateEntity(XodusFlagModel entity) {
        return updateEntity(entity, false);
    }

    @Override
    public Optional<XodusFlagModel> updateEntityAndFlush(XodusFlagModel entity) {
        return updateEntity(entity, true);
    }

    private Optional<XodusFlagModel> updateEntity(XodusFlagModel entity, boolean individualTransaction) {
        AtomicReference<XodusFlagModel> updatedModel = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(entity.getId(), storeTransaction);
            if (xodusEntity != null && XodusEntityTypes.FLAG.getType().equals(xodusEntity.getType())) {
                xodusEntity.setProperty(AbstractXodusModel.DATA_KEY, entity.getData());
                updatedModel.set(convertToFlagModel(xodusEntity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(updatedModel.get());
    }

    @XodusTransaction
    @Override
    public boolean removeEntity(String id) {
        return removeEntity(id, false);
    }

    @Override
    public boolean removeEntityAndFlush(String id) {
        return removeEntity(id, true);
    }

    @SuppressWarnings("DuplicatedCode")
    private boolean removeEntity(String id, boolean individualTransaction) {
        AtomicBoolean result = new AtomicBoolean();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(id, storeTransaction);

            if (xodusEntity != null && XodusEntityTypes.FLAG.getType().equals(xodusEntity.getType())) {
                result.set(xodusEntity.delete());
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return result.get();
    }

    @Nullable
    private Entity getXodusEntity(String id, StoreTransaction storeTransaction) {
        Entity entity = null;
        try {
            entity = storeTransaction.getEntity(storeTransaction.toEntityId(id));
        } catch (EntityRemovedInDatabaseException entityRemovedInDatabaseException) {
            log.warn("Entity was not found, id = " + id);
        }
        return entity;
    }

    private XodusFlagModel convertToFlagModel(Entity xodusEntity) {
        Boolean result = (Boolean) xodusEntity.getProperty(AbstractXodusModel.DATA_KEY);
        return XodusFlagModel.builder()
                .id(xodusEntity.getId().toString())
                .data(result != null && result)
                .build();
    }
}
