package com.web.localstorage.app.manage.service;

import jetbrains.exodus.entitystore.StoreTransaction;

import java.util.function.Consumer;

public interface XodusStoreTransactionService {

    void doInStoreTransaction(Consumer<StoreTransaction> consumer);

    void doInIndividualStoreTransaction(Consumer<StoreTransaction> consumer);
}
