package com.web.localstorage.app.manage.service;

import jetbrains.exodus.entitystore.PersistentEntityStore;
import jetbrains.exodus.entitystore.StoreTransaction;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
public class XodusStoreTransactionServiceImpl implements XodusStoreTransactionService {

    private final PersistentEntityStore entityStore;

    @Override
    public void doInStoreTransaction(Consumer<StoreTransaction> consumer) {
        StoreTransaction storeTransaction = entityStore.getCurrentTransaction();
        if (storeTransaction == null) {
            doInIndividualStoreTransaction(consumer);
        } else {
            consumer.accept(storeTransaction);
        }
    }

    @Override
    public void doInIndividualStoreTransaction(Consumer<StoreTransaction> consumer) {
        StoreTransaction storeTransaction = entityStore.beginExclusiveTransaction();
        consumer.accept(storeTransaction);
        storeTransaction.commit();
    }
}
