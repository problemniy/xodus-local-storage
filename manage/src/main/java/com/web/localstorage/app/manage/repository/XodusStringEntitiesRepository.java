package com.web.localstorage.app.manage.repository;

import com.web.localstorage.app.database.model.XodusStringModel;
import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import com.web.localstorage.app.database.type.XodusEntityTypes;
import com.web.localstorage.app.manage.annotation.XodusTransaction;
import com.web.localstorage.app.manage.service.XodusStoreTransactionService;
import jakarta.annotation.Nullable;
import jetbrains.exodus.entitystore.Entity;
import jetbrains.exodus.entitystore.EntityRemovedInDatabaseException;
import jetbrains.exodus.entitystore.StoreTransaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Repository
@Slf4j
@RequiredArgsConstructor
public class XodusStringEntitiesRepository implements XodusEntitiesRepository<XodusStringModel, String> {

    private final XodusStoreTransactionService transactionService;

    @Override
    public Collection<XodusStringModel> getAllEntities() {
        return getAllEntities(false);
    }

    @Override
    public Collection<XodusStringModel> getAllEntitiesAndFlush() {
        return getAllEntities(true);
    }

    private Collection<XodusStringModel> getAllEntities(boolean individualTransaction) {
        Collection<XodusStringModel> models = new ArrayList<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> storeTransaction.getAll(XodusEntityTypes.STRING.getType())
                .forEach(entity -> models.add(convertToStringModel(entity)));

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return models;
    }

    @XodusTransaction
    @Override
    public Optional<XodusStringModel> createEntity(String value) {
        return createEntity(value, false);
    }

    @Override
    public Optional<XodusStringModel> createEntityAndFlush(String value) {
        return createEntity(value, true);
    }

    private Optional<XodusStringModel> createEntity(String value, boolean individualTransaction) {
        AtomicReference<Optional<XodusStringModel>> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = storeTransaction.newEntity(XodusEntityTypes.STRING.getType());
            entity.setProperty(AbstractXodusModel.DATA_KEY, value);
            model.set(Optional.ofNullable(convertToStringModel(entity)));
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return model.get();
    }

    @XodusTransaction
    @Override
    public Optional<XodusStringModel> getEntity(String id) {
        return getEntity(id, false);
    }

    @Override
    public Optional<XodusStringModel> getEntityAndFlush(String id) {
        return getEntity(id, true);
    }

    private Optional<XodusStringModel> getEntity(String id, boolean individualTransaction) {
        AtomicReference<XodusStringModel> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = getXodusEntity(id, storeTransaction);
            if (entity != null && XodusEntityTypes.STRING.getType().equals(entity.getType())) {
                model.set(convertToStringModel(entity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(model.get());
    }

    @XodusTransaction
    @Override
    public Optional<XodusStringModel> updateEntity(XodusStringModel entity) {
        return updateEntity(entity, false);
    }

    @Override
    public Optional<XodusStringModel> updateEntityAndFlush(XodusStringModel entity) {
        return updateEntity(entity, true);
    }

    private Optional<XodusStringModel> updateEntity(XodusStringModel entity, boolean individualTransaction) {
        AtomicReference<XodusStringModel> updatedModel = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(entity.getId(), storeTransaction);
            if (xodusEntity != null && XodusEntityTypes.STRING.getType().equals(xodusEntity.getType())) {
                xodusEntity.setProperty(AbstractXodusModel.DATA_KEY, entity.getData());
                updatedModel.set(convertToStringModel(xodusEntity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(updatedModel.get());
    }

    @XodusTransaction
    @Override
    public boolean removeEntity(String id) {
        return removeEntity(id, false);
    }

    @Override
    public boolean removeEntityAndFlush(String id) {
        return removeEntity(id, true);
    }

    @SuppressWarnings("DuplicatedCode")
    private boolean removeEntity(String id, boolean individualTransaction) {
        AtomicBoolean result = new AtomicBoolean();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(id, storeTransaction);

            if (xodusEntity != null && XodusEntityTypes.STRING.getType().equals(xodusEntity.getType())) {
                result.set(xodusEntity.delete());
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return result.get();
    }

    @Nullable
    private Entity getXodusEntity(String id, StoreTransaction storeTransaction) {
        Entity entity = null;
        try {
            entity = storeTransaction.getEntity(storeTransaction.toEntityId(id));
        } catch (EntityRemovedInDatabaseException entityRemovedInDatabaseException) {
            log.warn("Entity was not found, id = " + id);
        }
        return entity;
    }

    private XodusStringModel convertToStringModel(Entity xodusEntity) {
        return XodusStringModel.builder()
                .id(xodusEntity.getId().toString())
                .data((String) xodusEntity.getProperty(AbstractXodusModel.DATA_KEY))
                .build();
    }
}
