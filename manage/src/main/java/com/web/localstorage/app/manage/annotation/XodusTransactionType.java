package com.web.localstorage.app.manage.annotation;

public enum XodusTransactionType {
    READ, WRITE
}
