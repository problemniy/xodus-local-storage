package com.web.localstorage.app.manage.repository;

import com.web.localstorage.app.database.model.XodusNumberModel;
import com.web.localstorage.app.database.model.abs.AbstractXodusModel;
import com.web.localstorage.app.database.type.XodusEntityTypes;
import com.web.localstorage.app.manage.annotation.XodusTransaction;
import com.web.localstorage.app.manage.annotation.XodusTransactionType;
import com.web.localstorage.app.manage.service.XodusStoreTransactionService;
import jakarta.annotation.Nullable;
import jetbrains.exodus.entitystore.Entity;
import jetbrains.exodus.entitystore.EntityRemovedInDatabaseException;
import jetbrains.exodus.entitystore.StoreTransaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Repository
@Slf4j
@RequiredArgsConstructor
public class XodusNumberEntitiesRepository implements XodusEntitiesRepository<XodusNumberModel, Long> {

    private final XodusStoreTransactionService transactionService;

    @Override
    public Collection<XodusNumberModel> getAllEntities() {
        return getAllEntities(false);
    }

    @Override
    public Collection<XodusNumberModel> getAllEntitiesAndFlush() {
        return getAllEntities(true);
    }

    private Collection<XodusNumberModel> getAllEntities(boolean individualTransaction) {
        Collection<XodusNumberModel> models = new ArrayList<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> storeTransaction.getAll(XodusEntityTypes.NUMBER.getType())
                .forEach(entity -> models.add(convertToNumberModel(entity)));

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return models;
    }

    @XodusTransaction
    @Override
    public Optional<XodusNumberModel> createEntity(Long value) {
        return createEntity(value, false);
    }

    @Override
    public Optional<XodusNumberModel> createEntityAndFlush(Long value) {
        return createEntity(value, true);
    }

    private Optional<XodusNumberModel> createEntity(Long value, boolean individualTransaction) {
        AtomicReference<Optional<XodusNumberModel>> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = storeTransaction.newEntity(XodusEntityTypes.NUMBER.getType());
            entity.setProperty(AbstractXodusModel.DATA_KEY, value);
            model.set(Optional.ofNullable(convertToNumberModel(entity)));
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return model.get();
    }

    @XodusTransaction(XodusTransactionType.READ)
    @Override
    public Optional<XodusNumberModel> getEntity(String id) {
        return getEntity(id, false);
    }

    @Override
    public Optional<XodusNumberModel> getEntityAndFlush(String id) {
        return getEntity(id, true);
    }

    private Optional<XodusNumberModel> getEntity(String id, boolean individualTransaction) {
        AtomicReference<XodusNumberModel> model = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity entity = getXodusEntity(id, storeTransaction);
            if (entity != null && XodusEntityTypes.NUMBER.getType().equals(entity.getType())) {
                model.set(convertToNumberModel(entity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(model.get());
    }

    @XodusTransaction
    @Override
    public Optional<XodusNumberModel> updateEntity(XodusNumberModel entity) {
        return updateEntity(entity, false);
    }

    @Override
    public Optional<XodusNumberModel> updateEntityAndFlush(XodusNumberModel entity) {
        return updateEntity(entity, true);
    }

    private Optional<XodusNumberModel> updateEntity(XodusNumberModel entity, boolean individualTransaction) {
        AtomicReference<XodusNumberModel> updatedModel = new AtomicReference<>();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(entity.getId(), storeTransaction);
            if (xodusEntity != null && XodusEntityTypes.NUMBER.getType().equals(xodusEntity.getType())) {
                xodusEntity.setProperty(AbstractXodusModel.DATA_KEY, entity.getData());
                updatedModel.set(convertToNumberModel(xodusEntity));
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return Optional.ofNullable(updatedModel.get());
    }

    @XodusTransaction
    @Override
    public boolean removeEntity(String id) {
        return removeEntity(id, false);
    }

    @Override
    public boolean removeEntityAndFlush(String id) {
        return removeEntity(id, true);
    }

    @SuppressWarnings("DuplicatedCode")
    private boolean removeEntity(String id, boolean individualTransaction) {
        AtomicBoolean result = new AtomicBoolean();
        Consumer<StoreTransaction> consumer = storeTransaction -> {
            Entity xodusEntity = getXodusEntity(id, storeTransaction);

            if (xodusEntity != null && XodusEntityTypes.NUMBER.getType().equals(xodusEntity.getType())) {
                result.set(xodusEntity.delete());
            }
        };

        if (individualTransaction) {
            transactionService.doInIndividualStoreTransaction(consumer);
        } else {
            transactionService.doInStoreTransaction(consumer);
        }

        return result.get();
    }

    @Nullable
    private Entity getXodusEntity(String id, StoreTransaction storeTransaction) {
        Entity entity = null;
        try {
            entity = storeTransaction.getEntity(storeTransaction.toEntityId(id));
        } catch (EntityRemovedInDatabaseException entityRemovedInDatabaseException) {
            log.warn("Entity was not found, id = " + id);
        }
        return entity;
    }

    private XodusNumberModel convertToNumberModel(Entity xodusEntity) {
        return XodusNumberModel.builder()
                .id(xodusEntity.getId().toString())
                .data((Long) xodusEntity.getProperty(AbstractXodusModel.DATA_KEY))
                .build();
    }
}
