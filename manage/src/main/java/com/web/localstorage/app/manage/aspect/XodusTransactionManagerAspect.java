package com.web.localstorage.app.manage.aspect;

import com.web.localstorage.app.manage.annotation.XodusTransaction;
import com.web.localstorage.app.manage.annotation.XodusTransactionType;
import jetbrains.exodus.entitystore.PersistentEntityStore;
import jetbrains.exodus.entitystore.StoreTransaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Slf4j
@RequiredArgsConstructor
@Component
public class XodusTransactionManagerAspect {

    private final PersistentEntityStore entityStore;

    @Around("@annotation(com.web.localstorage.app.manage.annotation.XodusTransaction)")
    public Object supportTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
        XodusTransactionType transactionType = getTransactionType(joinPoint);
        Object result = null;
        StoreTransaction txn = null;

        try {
            txn = entityStore.getCurrentTransaction();
            boolean transactionExists = false;
            if (txn != null) {
                transactionExists = true;
            } else {
                txn = XodusTransactionType.READ == transactionType ? entityStore.beginReadonlyTransaction() : entityStore.beginTransaction();
            }

            result = joinPoint.proceed();

            if (!transactionExists) {
                if (XodusTransactionType.READ == transactionType) {
                    txn.abort();
                } else {
                    txn.commit();
                }
            }
        } catch (Exception exc) {
            if (txn != null) {
                txn.abort();
            }
            log.error("Rollback transaction, reason:\n", exc);
        }

        return result;
    }

    private XodusTransactionType getTransactionType(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        XodusTransaction xodusTransaction = method.getAnnotation(XodusTransaction.class);
        if (xodusTransaction != null) {
            return xodusTransaction.value();
        }
        return XodusTransactionType.WRITE;
    }
}
