package com.web.localstorage.app.manage.repository;

import com.web.localstorage.app.database.model.abs.AbstractXodusModel;

import java.util.Collection;
import java.util.Optional;

public interface XodusEntitiesRepository<T extends AbstractXodusModel<A>, A extends Comparable<?>> {

    Collection<T> getAllEntities();

    Collection<T> getAllEntitiesAndFlush();

    Optional<T> createEntity(A value);

    Optional<T> createEntityAndFlush(A value);

    Optional<T> getEntity(String id);

    Optional<T> getEntityAndFlush(String id);

    Optional<T> updateEntity(T entity);

    Optional<T> updateEntityAndFlush(T entity);

    boolean removeEntity(String id);

    boolean removeEntityAndFlush(String id);
}
